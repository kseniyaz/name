import matplotlib.pyplot as plt
import numpy as np
fir = []
sec = []
times = []
with open("sputnik.txt") as sput:
	for line in sput.readlines():
		if line != '\n':
			fir.append(line.split()[1])
			sec.append(line.split()[2])
			if (line.split()[0] == 'R') or (line.split()[0] == 'G'):
				times.append(line.split()[11])
			else:
				times.append(line.split()[10])
time = []
r = []
for i in times:
	r = i.split(':')
	time.append(float(r[0])*60*60+float(r[1])*60+float(r[2]))   

fig, ax = plt.subplots(1,1, figsize=(20,10))

ax.scatter(time, fir, marker='.', label='tecL vs time', color='blue')
ax.scatter(time, sec, marker='.', label="tecC vs time", color='red')
ax.set_ylabel('tecC, tecL')
ax.set_xlabel('time')
ax.legend()
plt.savefig("tec-time.png")
plt.show()
