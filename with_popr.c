#include <stdio.h>
#include <math.h>
#include <string.h >
#include <stdlib.h>
#include <gmp.h>
#include <assert.h>
#include <limits.h>
#define ugle 0.000072921151467
#define speed_of_light 299792458
#define pi  3.1415926535

/*
double f(double x, double jj)
{
    return x*x- jj;
}
double method_chord(double x_prev, double x_curr, double epsi, double jj)
{
    double x_next = 0;
    double tmp;

    do
    {
        tmp = x_next;
        x_next = x_curr - f(x_curr,jj) * (x_prev - x_curr) / (f(x_prev,jj) - f(x_curr,jj));
        x_prev = x_curr;
        x_curr = tmp;
    } while (abs(x_next - x_curr) > epsi);

    return x_next;
}

*/

double ex_an(double e, double M, double eps)
{
  double E = 0;
  double rez = 100;
  while (abs(rez - E) > eps)
    {
        rez = E;
        E = M + e*sin(E);
    }

  return E;
}
int main(void) {

    FILE *nav;
    FILE *coord_sput;
    FILE *pseudo;
    FILE *tt;
    coord_sput = fopen("coord_sput.txt", "w");
    pseudo = fopen("pseudo.txt", "r");
    nav = fopen("vost_0618a.16N", "r");
    tt = fopen("tt.txt", "w");
	char str[82];
	if (nav == NULL)       printf("isn't open");

	for (int i=0; i<7; i++)
    {
        fgets(str, 81 , nav);
    }
    int number[400], year[400], month[400], day[400], hour[400], minute[400], seconds[400];
    char IODE1[23], Crs1[20], delta_n1[20], M01[20], popr1[20];
    double IODE[400],Crs[400], delta_n[400], M0[400],popr[400];
    char Cus1[23], e1[20], Cuc1[20], A1[20];
    double Cus[400], e[400], Cuc[400], A[400];
    char Toe1[23], Cic1[20], Omega01[20], Cis1[20];
    double Toe[400], Cic[400], Omega0[400], Cis[400];
    char i01[23], Crc1[20], w01[20], Omega_p1[20];
    double i0[400], Crc[400], w0[400], Omega_p[400];
    char IDOT1[23], WN1[20];
    double IDOT[400], WN[400];
    // Чтение данных из файла
    for (int i = 0; i < 241; i++)
    {
        fgets(str, 21 , nav);
        sscanf(str, "%d%d%d%d%d%d%d", &number[i],&day[i], &month[i], &year[i], &hour[i], &minute[i], &seconds[i]);
        fgets(str, 3 , nav);
        fgets(str, 20 , nav);
        sscanf(str, "%s", &popr1);
        for(int j = 0; j < strlen(popr1); j++)
        {
            if (popr1[j] == 'D')   popr1[j] = 'e';
        }
        popr[i] = (atof(popr1));
        printf("%d Time: %d.%d.%d    %d:%d:%d.00, popravka chasov %.9lf\n\n", number[i],day[i], month[i], year[i], hour[i], minute[i], seconds[i], popr[i]);
        fprintf(tt, " dtt[%d] = %.12lf;\n",i,popr[i]);
        fgets(str, 40, nav);
        fgets(str, 23, nav);
        sscanf(str, "%s", &IODE1);
        for(int j = 0; j < strlen(IODE1); j++)
        {
            if (IODE1[j] == 'D')   IODE1[j] = 'e';
        }
        fgets(str, 20, nav);
        sscanf(str, "%s", &Crs1);
        for(int j = 0; j < strlen(Crs1); j++)
        {
            if (Crs1[j] == 'D')   Crs1[j] = 'e';
        }
        fgets(str, 20, nav);
        sscanf(str, "%s", &delta_n1);
        for(int j = 0; j < strlen(delta_n1); j++)
        {
            if (delta_n1[j] == 'D')   delta_n1[j] = 'e';
        }
        fgets(str, 20, nav);
        sscanf(str, "%s", &M01);
        for(int j = 0; j < strlen(M01); j++)
        {
            if (M01[j] == 'D')   M01[j] = 'e';
        }
        IODE[i] = (atof(IODE1));
        Crs[i] = (atof(Crs1));
        delta_n[i] = atof(delta_n1);
        M0[i] = atof(M01);
        printf("IODE = %.12lf, Crs = %.12lf, delta_n = %.12lf, M0 = %.12lf\n", IODE[i], Crs[i], delta_n[i], M0[i]);
        fprintf(tt, "IODE[%d] = %.12lf, Crs = %.12lf, delta_n = %.12lf, M0 = %.12lf\n", i, IODE[i], Crs[i], delta_n[i], M0[i]);
        fgets(str, 2, nav);
        fgets(str, 24, nav);

        sscanf(str, "%s", &Cuc1);
        for(int j = 0; j < strlen(Cuc1); j++)
        {
            if (Cuc1[j] == 'D')   Cuc1[j] = 'e';
        }
        fgets(str, 20, nav);
        sscanf(str, "%s", &e1);
        for(int j = 0; j < strlen(e1); j++)
        {
            if (e1[j] == 'D')   e1[j] = 'e';
        }
        fgets(str, 20, nav);
        sscanf(str, "%s", &Cus1);
        for(int j = 0; j < strlen(Cus1); j++)
        {
            if (Cus1[j] == 'D')   Cus1[j] = 'e';
        }
        fgets(str, 20, nav);
        sscanf(str, "%s", &A1);
        for(int j = 0; j < strlen(A1); j++)
        {
            if (A1[j] == 'D')   A1[j] = 'e';
        }

        Cuc[i] = (atof(Cuc1));
        e[i] = (atof(e1));
        Cus[i] = atof(Cus1);
        A[i] = atof(A1);
        printf("Cuc = %.12lf, e = %.12lf, Cus = %.12lf, A = %.12lf\n", Cuc[i], e[i], Cus[i], A[i]);
        fprintf(tt,"Cuc = %.12lf, e = %.12lf, Cus = %.12lf, A = %.12lf\n", Cuc[i], e[i], Cus[i], A[i]);

        fgets(str, 2, nav);
        fgets(str, 24, nav);

        sscanf(str, "%s", &Toe1);
        for(int j = 0; j < strlen(Toe1); j++)
        {
            if (Toe1[j] == 'D')   Toe1[j] = 'e';
        }
        fgets(str, 19, nav);
        sscanf(str, "%s", &Cic1);
        for(int j = 0; j < strlen(Cic1); j++)
        {
            if (Cic1[j] == 'D')   Cic1[j] = 'e';
        }

        fgets(str, 20, nav);
        sscanf(str, "%s", &Omega01);
        for(int j = 0; j < strlen(Omega01); j++)
        {
            if (Omega01[j] == 'D')   Omega01[j] = 'e';
        }
        fgets(str, 20, nav);
        sscanf(str, "%s", &Cis1);
        for(int j = 0; j < strlen(Cis1); j++)
        {
            if (Cis1[j] == 'D')   Cis1[j] = 'e';
        }

        Toe[i] = (atof(Toe1));
        Cic[i] = (atof(Cic1));
        Omega0[i] = atof(Omega01);
        Cis[i] = atof(Cis1);
        printf("Toe = %.12lf, Cic = %.12lf, Omega0 = %.12lf, Cis = %.12lf\n", Toe[i], Cic[i], Omega0[i], Cis[i]);
        fprintf(tt,"Toe = %.12lf, Cic = %.12lf, Omega0 = %.12lf, Cis = %.12lf\n", Toe[i], Cic[i], Omega0[i], Cis[i]);
        fgets(str, 2, nav);
        fgets(str, 22, nav);

        sscanf(str, "%s", &i01);
        for(int j = 0; j < strlen(i01); j++)
        {
            if (i01[j] == 'D')   i01[j] = 'e';
        }
        fgets(str, 20, nav);
        sscanf(str, "%s", &Crc1);
        for(int j = 0; j < strlen(Crc1); j++)
        {
            if (Crc1[j] == 'D')   Crc1[j] = 'e';
        }
        fgets(str, 20, nav);
        sscanf(str, "%s", &w01);
        for(int j = 0; j < strlen(w01); j++)
        {
            if (w01[j] == 'D')   w01[j] = 'e';
        }
        fgets(str, 20, nav);
        sscanf(str, "%s", &Omega_p1);
        for(int j = 0; j < strlen(Omega_p1); j++)
        {
            if (Omega_p1[j] == 'D')   Omega_p1[j] = 'e';
        }
        i0[i] = (atof(i01));
        Crc[i] = (atof(Crc1));
        w0[i] = atof(w01);
        Omega_p[i] = atof(Omega_p1);
        printf("Toe = %.12lf, Cic = %.12lf, Omega0 = %.12lf, Cis = %.12lf\n", i0[i], Crc[i], w0[i], Omega_p[i]);
        fprintf(tt,"Toe = %.12lf, Cic = %.12lf, Omega0 = %.12lf, Cis = %.12lf\n", i0[i], Crc[i], w0[i], Omega_p[i]);
        fgets(str, 2, nav);
        fgets(str, 23, nav);

        sscanf(str, "%s", &IDOT1);
        for(int j = 0; j < strlen(IDOT1); j++)
        {
            if (IDOT1[j] == 'D')   IDOT1[j] = 'e';
        }
        fgets(str, 20, nav);
        fgets(str, 20, nav);
        sscanf(str, "%s", &WN1);
        for(int j = 0; j < strlen(WN1); j++)
        {
            if (WN1[j] == 'D')   WN1[j] = 'e';
        }
        IDOT[i] = (atof(IDOT1));
        WN[i] = (atof(WN1));
        printf("IDOT = %.12lf, WN = %.12lf\n", IDOT[i], WN[i]);
        fprintf(tt,"IDOT = %.12lf, WN = %.12lf\n", IDOT[i], WN[i]);
        fgets(str, 25, nav);
        fgets(str, 81, nav);
        fgets(str, 43, nav);
    }

    char buff[40];
    char sat_name[4];
    int sat_numb[2*60*24*8], time[2*60*24*8];
    double p[2*60*24*8];
    for (int i = 0; i < 2*24*60*7+1873; i++)
    {
        fgets(sat_name, 5 , pseudo);
        buff[0] = sat_name[1];
        buff[1] = sat_name[2] ;
        sscanf(buff, "%d", &sat_numb[i]);

        fgets(buff, 16, pseudo);
        sscanf(buff, "%lf", &p[i]);

        fgets(buff, 9, pseudo);
        sscanf(buff, "%d", &time[i]);
        fgets(buff,2,pseudo);
    }

    double mu = 19964980.3857;
    double v[400], r[400], ii[400], u[400];
    double MS[400], E[400];
    double n0[400], n[400],  f[400];
    double Tem[400], t[400];
    double delu[400], delr[400], deli[400];
    double  Xorb[400], Yorb[400], x[400], y[400], z[400];
    double  ugl[400],  ugl0[400], uglp[400];
    // Нахождение коордиинат спутников
    for (int j = 0; j < 241; j++)
    {
        for (int i = 0; i < 2*24*60*7+1873; i++)
        {
            if ((sat_numb[i] == number[j])&& (60*60*hour[j]+ 60*minute[j]+seconds[j] == time[i]))
            {
                //printf("\nprov %d %d %d %d\n\n",sat_numb[i], number[j],60*60*hour[j]+ 60*minute[j]+seconds[j] , time[i]);
                n0[j] = mu/pow(A[j],3);
                Tem[j] = time[i] - p[i]/speed_of_light+ (24 * 60 * 60)*((day[j]  + ((31 * (month[j] + 12)) / 12) + year[j]+2000 + ((2000+year[j])/ 4) - ((2000+year[j]) / 100) + ((year[j]+2000) / 400))%7);
                t[j] = Tem[j] - Toe[j];
                if (t[j] > 302400)  t[j] = t[j] - 604800;
                else if (t[j] < -302400)    t[j] = t[j] + 604800;
                n[j] = n0[j] + delta_n[j];
                printf("Tem %lf\nt %lf\n", Tem[j],t[j]);
                MS[j] = M0[j] + n[j]*t[j];
                if (MS[j] > 2*pi)
                {
                    while(MS[j] > 2*pi)
                    {
                        MS[j] = MS[j] - (2*pi);
                    }
                }
                 if (MS[j] < -2*pi)
            {
                while(MS[j] < -2*pi)
                {
                    MS[j] = MS[j] + (2*pi);

                }
            }
                E[j] = ex_an(e[j], MS[j], 0.00000001);
                v[j] = 2*atan(pow(((1+e[j])/(1-e[j])),(1/2))*(tan(E[j]/2)));
                f[j] = v[j] + w0[j];
                printf("n %lf\nM %lf\nE %lf\nv %.9lf\nf %lf\n\n",n[j], MS[j], E[j], v[j], f[j]);
                delu[j] = Cus[j]*sin(2*f[j])+Cuc[j]*cos(2*f[j]);
                delr[j] = Crs[j]*sin(2*f[j])+Crc[j]*cos(2*f[j]);
                deli[j] = Cis[j]*sin(2*f[j])+Cic[j]*cos(2*f[j]);

                u[j] = f[j] + delu[j];
                r[j] = A[j]*A[j]*(1 - e[j]*cos(E[j])) + delr[j];
                ii[j] = i0[j] + deli[j] + IDOT[j]*t[j];

                Xorb[j] = r[j]*cos(u[j]);
                Yorb[j] = r[j]* sin(u[j]);

                ugl[j] = Omega0[j] +t[j]*(Omega_p[j] - ugle) - ugle*Toe[j];

                x[j] = Xorb[j]*cos(ugl[j]) - Yorb[j]*cos(ii[j])*sin(ugl[j]);
                y[j] = Xorb[j]*sin(ugl[j]) + Yorb[j]*cos(ii[j])*cos(ugl[j]);
                z[j] = Yorb[j]*sin(ii[j]);

                fprintf(coord_sput, "%3d %8d %16lf %16lf %16lf\n",number[j],time[i],x[j], y[j], z[j]);

            }
        }
    }
    double dp[4], dx[4], dy[4], dz[4], sp[4], sx[4], sy[4], sz[4];
    double a[3], D, D1, D2, D3, dd1, dd2, dd3;
    double xsh, ysh, zsh, ax, ay, az;
    double dx4sh, dy4sh, dz4sh;
    double a4, b4, c4, epsilon, eps1, eps2, dt1, dt2;
    double xx, yy, zz;
    double ee, eesh, aa = 6378137.0, bb = 6356752.0;
    double lambda, teta, phi;
    double xn[4], yn[4], zn[4];
    double pp[4];
    int m = 0;
    //Нахождение координат приемника
    for (int j = 30; j < 241; j++)
        {
            for (int k = 0; k < 2*24*60*7+1873; k++)
            {
                if ((sat_numb[k] == number[j]) && (60*60*hour[j]+ 60*minute[j]+seconds[j] == time[k]))
                {
                    pp[m] = p[k] + speed_of_light*popr[j];
                    xn[m] = x[k];
                    yn[m] = y[k];
                    zn[m] = z[k];
                    m++;
                    if (m == 4)
                    {
                        break;
                    }
                }
            }
        }
    for (int i = 0; i < 3; i++)
    {
        dp[i] = (pp[i+1] - pp[i]);
        dx[i] = (xn[i+1] - xn[i]);
        dy[i] = (yn[i+1] - yn[i]);
        dz[i] = (zn[i+1] - zn[i]);
        sp[i] = (pp[i+1] + pp[i]);
        sx[i] = (xn[i+1] + xn[i]);
        sy[i] = (yn[i+1] + yn[i]);
        sz[i] = (zn[i+1] + zn[i]);
    }
    for (int i= 0; i < 3; i++)
     {
        a[i] = (dx[i] * sx[i] + dy[i] * sy[i] + dz[i] * sz[i] - dp[i] * sp[i]) / 2;
     }

    D = dx[0]*dy[1]*dz[2] + dy[0]*dz[1]*dx[2]  + dx[1]*dy[2]*dz[0]  - dz[0]*dy[1]*dx[2] - dx[1]*dy[0]*dz[2] - dy[2]*dz[1]*dx[0] ;
    D1 = a[0]*dy[1]*dz[2] + dy[0]*dz[1]*a[2] + a[1]*dy[2]*dz[0] - dz[0]*dy[1]*a[2] - a[1]*dy[0]*dz[2] - dy[2]*dz[1]*a[0] ;
    D2 = dx[0]*a[1]*dz[2] + a[0]*dz[1]*dx[2] + dx[1]*a[2]*dz[0] - dz[0]*a[1]*dx[2] - dx[1]*a[0]*dz[2] - a[2]*dz[1]*dx[0];
    D3 = dx[0]*dy[1]*a[2] + dy[0]*a[1]*dx[2] + dx[1]*dy[2]*a[0] - a[0]*dy[1]*dx[2] - dx[1]*dy[0]*a[2] - dy[2]*a[1]*dx[0];
    dd1 = -(dp[0]*dy[1]*dz[2] + dy[0]*dz[1]*dp[2] + dp[1]*dy[2]*dz[0] - dz[0]*dy[1]*dp[2] - dp[1]*dy[0]*dz[2] - dy[2]*dz[1]*dp[0]) ;
    dd2 = -(dx[0]*dp[1]*dz[2] + dp[0]*dz[1]*dx[2] + dx[1]*dp[2]*dz[0] - dz[0]*dp[1]*dx[2] - dx[1]*dp[0]*dz[2] - dp[2]*dz[1]*dx[0]);
    dd3 = -(dx[0]*dy[1]*dp[2] + dy[0]*dp[1]*dx[2] + dx[1]*dy[2]*dp[0] - dp[0]*dy[1]*dx[2] - dx[1]*dy[0]*dp[2] - dy[2]*dp[1]*dx[0]);
    xsh = D1/D; ysh = D2/D; zsh = D3/D;

    ax = dd1/D; ay = dd2/D; az = dd3/D;
    dx4sh = xn[3] - xsh;
    dy4sh = yn[3] - ysh;
    dz4sh = zn[3] - zsh;
    a4 = 1 - ax*ax - ay*ay - az*az;
    b4 = pp[3] + dx4sh*ax + dy4sh*ay + dz4sh*az;
    c4 = - dx4sh*dx4sh - dy4sh*dy4sh - dz4sh*dz4sh + pp[3]*pp[3];
    c4 = c4/2;
    double mm = 0;
    mm = sqrt((b4*b4 - a4*c4)/(a4*a4));
    eps1 = - b4/a4 + mm;
    eps2 = - b4/a4 - mm;
    dt1 = eps1/speed_of_light;
    dt2 = eps2/speed_of_light;
    if (fabs(dt1) <= fabs(dt2))	epsilon = eps1;
    else	epsilon = eps2;
    xx = xsh + epsilon*ax;
    yy = ysh + epsilon*ay;
    zz = zsh + epsilon*az;
    printf("x = %lf\ny = %lf\nz = %lf\n",xx,yy,zz);
    ee = (aa * aa - bb * bb) / (aa * aa);
    eesh = (aa * aa - bb * bb) / (bb * bb);
    lambda = atan2(yy, xx)* 180.0 / pi;
    teta = atan2(aa * zz , (bb * pow((xx * xx + yy * yy),0.5)))* 180.0 / pi;
    phi = atan2((zz + eesh * eesh * bb * pow((sin(teta)),3)) , (pow((xx * xx + yy * yy),0.5)- ee * aa * pow(cos(teta),3)))* 180.0 / pi;

    printf("lambda = %lf\nphi = %lf\n",lambda, phi);

    fclose(coord_sput);
    fclose(pseudo);
    fclose(nav);
    return 0;
}
