#include <stdio.h>
#include <math.h>
#include <string.h >
#include <stdlib.h>
#include <gmp.h>
#include <assert.h>
#include <limits.h>
#define ugle 0.000072921151467
#define speed_of_light 299792458

double ex_an(double e, double M, double eps)
{
  double E = 0;
  double rez = 100;
  while (abs(rez - E) > eps)
    {
        rez = E;
        E = M + e*sin(E);
    }

  return E;
}

int main(void) {

    /*
    int  year = 2006, month = 07, day = 10, hour = 11, minute = 53, seconds = 45;
    double IODE[4],Crs = -135.6875, delta_n = 3.83337396113*pow(10,-9), M0 = -1.78927512368;
    double Cus = 9.02265310287*pow(10,-6), e = 0.00963819015305, Cuc = -7.13393092155*pow(10,-6), A = 5153.62374496;
    double Toe = 129600.0, Cic = -1.86264514923*pow(10,-7), Omega0 = 2.73843067938, Cis = -5.21540641785*pow(10,-8);
    double i0 = 0.974034050279, Crc = 214.21875, w0 = 2.66223304346, Omega_p = -7.68603443990*pow(10,-9);
    double IDOT = 1.67864135072*pow(10,-11), WN = 1383.0;
    double mu = 19964980.3857;
    double v, r, ii, u, time = 129225;
    double MS, E;
    double n0, n,  f;
    double Tem, t;
    double delu, delr, deli;
    double  Xorb, Yorb, x, y, z;
    double  ugl,  ugl0, uglp, p = 21267472.262;

    n0 = mu/pow(A,3);
    Tem = time - p/speed_of_light ;
    t = Tem - Toe;
    if (t > 302400)  t = t - 604800;
    else if (t < -302400)    t = t+ 604800;
    n = n0 + delta_n;
    MS = M0 + n*t;
    E = ex_an(e, MS, 0.000001);
    v = (2*atan(pow(((1+e)/(1-e)),(1/2))*tan(E/2)));
    f = v + w0;
    delu = Cus*sin(2*f)+Cuc*cos(2*f);
    delr = Crs*sin(2*f)+Crc*cos(2*f);
    deli = Cis*sin(2*f)+Cic*cos(2*f);
    u = f + delu;
    r = A*A*(1 - e*cos(E)) + delr;
    ii = i0 + deli + IDOT*t;
    Xorb = r*cos(u);
    Yorb = r* sin(u);
    ugl = Omega0 +t*(Omega_p- ugle) - ugle*Toe;
    x = Xorb*cos(ugl) - Yorb*cos(ii)*sin(ugl);
    y= Xorb*sin(ugl) + Yorb*cos(ii)*cos(ugl);
    z= Yorb*sin(ii);
    */
    double dp[4], dx[4], dy[4], dz[4], sp[4], sx[4], sy[4], sz[4];
    double a[3], D, D1, D2, D3, dd1, dd2, dd3;
    double xsh, ysh, zsh, ax, ay, az;
    double dx4sh, dy4sh, dz4sh;
    double a4, b4, c4, epsilon, eps1, eps2, dt1, dt2;
    double xx, yy, zz;
    double ee, eesh, aa = 6378137.0, bb = 6356752.0, pi = 3.1415926535;;
    double lambda, teta, phi;
    int m = 0;
    double xn[] = {21277632.227,14136001.009,6380947.343,13667726.877};
    double yn[] = {2624628.197,-7530688.663,22063437.865,9545209.321};
    double zn[] = {15798292.012,21144548.488,13292356.357,21346910.550};
    double pp[] = {21267472.262,21406610.202,22193724.917,20826905.858};
    double dtt[] = {-0.000071967360017,0.0000863327302812,0.0000760678518091,0.0000573110823339};
    for (int i = 0; i < 3; i++)
    {
        pp[i] = pp[i] + dtt[i]*speed_of_light;
    }
    for (int i = 0; i < 3; i++)
    {
        dp[i] = (pp[i+1] - pp[i]);
        dx[i] = (xn[i+1] - xn[i]);
        dy[i] = (yn[i+1] - yn[i]);
        dz[i] = (zn[i+1] - zn[i]);
        sp[i] = (pp[i+1] + pp[i]);
        sx[i] = (xn[i+1] + xn[i]);
        sy[i] = (yn[i+1] + yn[i]);
        sz[i] = (zn[i+1] + zn[i]);

    }
    for (int i= 0; i < 3; i++)
     {
        a[i] = (dx[i] * sx[i] + dy[i] * sy[i] + dz[i] * sz[i] - dp[i] * sp[i]) / 2;

     }

    D = dx[0]*dy[1]*dz[2] + dy[0]*dz[1]*dx[2]  + dx[1]*dy[2]*dz[0]  - dz[0]*dy[1]*dx[2] - dx[1]*dy[0]*dz[2] - dy[2]*dz[1]*dx[0] ;
    D1 = a[0]*dy[1]*dz[2] + dy[0]*dz[1]*a[2] + a[1]*dy[2]*dz[0] - dz[0]*dy[1]*a[2] - a[1]*dy[0]*dz[2] - dy[2]*dz[1]*a[0] ;
    D2 = dx[0]*a[1]*dz[2] + a[0]*dz[1]*dx[2] + dx[1]*a[2]*dz[0] - dz[0]*a[1]*dx[2] - dx[1]*a[0]*dz[2] - a[2]*dz[1]*dx[0];
    D3 = dx[0]*dy[1]*a[2] + dy[0]*a[1]*dx[2] + dx[1]*dy[2]*a[0] - a[0]*dy[1]*dx[2] - dx[1]*dy[0]*a[2] - dy[2]*a[1]*dx[0];
    dd1 = -(dp[0]*dy[1]*dz[2] + dy[0]*dz[1]*dp[2] + dp[1]*dy[2]*dz[0] - dz[0]*dy[1]*dp[2] - dp[1]*dy[0]*dz[2] - dy[2]*dz[1]*dp[0]) ;
    dd2 = -(dx[0]*dp[1]*dz[2] + dp[0]*dz[1]*dx[2] + dx[1]*dp[2]*dz[0] - dz[0]*dp[1]*dx[2] - dx[1]*dp[0]*dz[2] - dp[2]*dz[1]*dx[0]);
    dd3 = -(dx[0]*dy[1]*dp[2] + dy[0]*dp[1]*dx[2] + dx[1]*dy[2]*dp[0] - dp[0]*dy[1]*dx[2] - dx[1]*dy[0]*dp[2] - dy[2]*dp[1]*dx[0]);
    xsh = D1/D; ysh = D2/D; zsh = D3/D;

    ax = dd1/D; ay = dd2/D; az = dd3/D;
    dx4sh = xn[3] - xsh;
    dy4sh = yn[3] - ysh;
    dz4sh = zn[3] - zsh;
    a4 = 1 - ax*ax - ay*ay - az*az;
    b4 = pp[3] + dx4sh*ax + dy4sh*ay + dz4sh*az;
    c4 = - dx4sh*dx4sh - dy4sh*dy4sh - dz4sh*dz4sh + pp[3]*pp[3];
    c4 = c4/2;
    //printf("%lf\n%lf \n\n",b4,c4);
    double mm = 0;
    mm = sqrt((b4*b4 - a4*c4)/(a4*a4));
    //printf("%lf mm\n\n",mm);
    eps1 = - b4/a4 + mm;
    eps2 = - b4/a4 - mm;

    dt1 = eps1/speed_of_light;
    dt2 = eps2/speed_of_light;
    if (abs(dt1) <= abs(dt2))	epsilon = eps1;
    else	epsilon = eps2;
    xx = xsh + epsilon*ax;
    yy = ysh + epsilon*ay;
    zz = zsh + epsilon*az;
    printf("x = %lf\ny = %lf\nz = %lf\n",xx,yy,zz);
    ee = (aa * aa - bb * bb) / (aa * aa);
    eesh = (aa * aa - bb * bb) / (bb * bb);
    //printf("%lf\n%lf \n\n",ee,eesh);
    lambda = atan2(yy , xx)* 180.0 / pi;
    teta = atan2 (aa * zz , (bb * pow((xx * xx + yy * yy),0.5)))* 180.0 / pi;
    phi = atan2 ((zz + eesh * eesh * bb * pow((sin(teta)),3)) , (pow((xx * xx + yy * yy),0.5)- ee * aa * pow(cos(teta),3))) * 180.0 / pi;

    printf("lam = %lf\nphi = %lf\n",lambda,  phi);
}
