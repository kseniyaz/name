#include <stdio.h>
#include <math.h>
#include <string.h >
#include <time.h>
#include <stdlib.h>
#define f1 1575420000
#define f2 1227600000
#define speed_of_light 299792458
#define pi  3.1415926535

	typedef struct {
        int year, month, day, hours, minutes, seconds;
    } date;

    typedef struct {
        date *time;
        double **c1, **p1, **l1, **c2, **p2, **l2, **tecC, **tecL;
        int *sat_numb;
        int capacity; //размер выделенной памяти
        int size; // число элементов в списке ofile
    } ofile;

void IncSize(ofile *oo)
{
     oo->size++;
     if ( oo->size > oo->capacity ) {
         oo->capacity += 100;
         oo->time = (double*)realloc ( oo->time, sizeof(double)*oo->capacity);
         oo->c1 = (double**)realloc ( oo->c1, sizeof(double*)*oo->capacity);
         oo->c2 = (double**)realloc ( oo->c2, sizeof(double*)*oo->capacity);
         oo->p1 = (double**)realloc ( oo->p1, sizeof(double*)*oo->capacity);
         oo->p2 = (double**)realloc ( oo->p2, sizeof(double*)*oo->capacity);
         oo->l1 = (double**)realloc ( oo->l1, sizeof(double*)*oo->capacity);
         oo->l2 = (double**)realloc ( oo->l2, sizeof(double*)*oo->capacity);
         oo->sat_numb = (double*)realloc ( oo->sat_numb, sizeof(double)*oo->capacity);
         for (int u = 0; u < oo->capacity; u++)
        {
             oo->c1[u] = (double*)realloc ( oo->c1[u], sizeof(double)*oo->capacity);
             oo->c2[u] = (double*)realloc ( oo->c2[u], sizeof(double)*oo->capacity);
             oo->p1[u] = (double*)realloc ( oo->p1[u], sizeof(double)*oo->capacity);
             oo->p2[u] = (double*)realloc ( oo->p2[u], sizeof(double)*oo->capacity);
             oo->l1[u] = (double*)realloc ( oo->l1[u], sizeof(double)*oo->capacity);
             oo->l2[u] = (double*)realloc ( oo->l2[u], sizeof(double)*oo->capacity);
         }
     }
     printf("\nsize = %d,  capacity = %d\n",oo->size, oo->capacity);
}

int main(void) {

    FILE *file;
    FILE *tecC_file;
    FILE *tecL_file;
    FILE *timef;
    FILE *python;
    FILE *pseudo;
    pseudo = fopen("pseudo.txt", "w");
    python = fopen("for_python.txt", "w");
    timef = fopen("time.txt", "w");
    tecC_file = fopen("tecC.txt", "w");
    tecL_file = fopen("tecL.txt", "w");
	file = fopen("vost_0618a.16o", "r");
	char str[82];
	if (file == NULL)       printf("isn't open");
	for (int i=0; i<76; i++)
    {
        fgets(str, 81 , file);
    }

    int sum_sec[2*60*24] = {0};

    ofile o;
    o.size = 0;
    o.capacity = 100;
    o.time = (date*)calloc(o.capacity, sizeof(date));
    o.sat_numb = (int*)calloc(o.capacity, sizeof(int));
    o.c1 = (double**)calloc(o.capacity, sizeof(double*));
    o.p1 = (double**)calloc(o.capacity, sizeof(double*));
    o.l1 = (double**)calloc(o.capacity, sizeof(double*));
    o.c2 = (double**)calloc(o.capacity, sizeof(double*));
    o.p2 = (double**)calloc(o.capacity, sizeof(double*));
    o.l2 = (double**)calloc(o.capacity, sizeof(double*));
    o.tecC = (double**)calloc(o.capacity, sizeof(double*));
    o.tecL = (double**)calloc(o.capacity, sizeof(double*));
    for (int r = 0; r < o.capacity; r++)
    {
            o.c1[r] = (double*)calloc(o.capacity, sizeof(double));
            o.p1[r] = (double*)calloc(o.capacity, sizeof(double));
            o.l1[r] = (double*)calloc(o.capacity, sizeof(double));
            o.c2[r] = (double*)calloc(o.capacity, sizeof(double));
            o.p2[r] = (double*)calloc(o.capacity, sizeof(double));
            o.l2[r] = (double*)calloc(o.capacity, sizeof(double));
            o.tecC[r] = (double*)calloc(o.capacity, sizeof(double));
            o.tecL[r] = (double*)calloc(o.capacity, sizeof(double));
    }
    char *sat_name_s[32];
	char sat_numb_str[3], sat_name[4];
	int j = -1;
	ofile *oo;
	oo = &o;

    while(!feof(file))
    {
        j++;
        sscanf(str, "%d%d%d%d%d%d", &o.time[j].year, &o.time[j].month, &o.time[j].day, &o.time[j].hours, &o.time[j].minutes, &o.time[j].seconds);
        printf("Time: %d.%d.%d    %d:%d:%d.00\n\n",  o.time[j].day, o.time[j].month, o.time[j].year, o.time[j].hours, o.time[j].minutes, o.time[j].seconds);
        sum_sec[j] += 60*60*o.time[j].hours + 60*o.time[j].minutes + o.time[j].seconds;
        sat_numb_str[0] = str[30];
        sat_numb_str[1] = str[31];
        sscanf(sat_numb_str, "%d", &o.sat_numb[j]);
        printf("number of satellites %d\n\n", o.sat_numb[j]);
        if (o.time[j].year != 16)   break;
        for (int i = 0; i < 12; i++)
        {
            sat_name[0] = str[32+3*i];
            sat_name[1] = str[33+3*i];
            sat_name[2] = str[34+3*i];
            sat_name_s[i] = malloc(strlen(sat_name));
            strcpy (sat_name_s[i],sat_name);
        }
        fgets(str, 81, file);
        fgets(str, 81, file);
        puts(str);
        for (int i = 0; i < (o.sat_numb[j]-12)%13; i++)
        {
            sat_name[0] = str[32+3*i];
            sat_name[1] = str[33+3*i];
            sat_name[2] = str[34+3*i];
            sat_name_s[i+12] = malloc(strlen(sat_name));
            strcpy (sat_name_s[i+12],sat_name);
        }

        if (o.sat_numb[j] >= 25)     {
                fgets(str, 81, file);
                puts(str);
                for (int i = 0; i < o.sat_numb[j] - 24; i++)
                {
                    sat_name[0] = str[32+3*i];
                    sat_name[1] = str[33+3*i];
                    sat_name[2] = str[34+3*i];
                    sat_name_s[i+24] = malloc(strlen(sat_name));
                    strcpy (sat_name_s[i+24],sat_name);
                }

        }

        for (int i=0; i < o.sat_numb[j]; i++)
        {
          IncSize(oo);
          o.c1[j][i] = o.p1[j][i] = o.l1[j][i] = o.c2[j][i] = o.p2[j][i] = o.l2[j][i] = 0;
          fgets(str, 81, file);
          sscanf(str, "%15lf     %15lf   %15lf    %*15lf    %15lf    %15lf", &o.c1[j][i], &o.p1[j][i], &o.l1[j][i], &o.c2[j][i], &o.p2[j][i]);
          fgets(str, 81, file);
          printf("%d %d i j \n",i,j);
          sscanf(str, "%15lf", &o.l2[j][i]);
          if (fabs(o.p2[j][i]) < 10e-7){
               continue;
          }
          printf("%s", sat_name_s[i]);
          printf("\n%15lf    %15lf   %15lf    %15lf    %15lf    %15lf\n", o.c1[j][i], o.p1[j][i], o.l1[j][i], o.c2[j][i], o.p2[j][i], o.l2[j][i]);
          fprintf(timef, "%d\n",sum_sec[j]);

        }
        fgets(str, 81, file);
    }
  for (int g = 0; g < j; j++)
  {
        free(o.p1[g]); free(o.p2[g]); free(o.c1[g]); free(o.c2[g]); free(o.l1[g]); free(o.l2[g]);
  }
  free(o.p1); free(o.p2); free(o.c1); free(o.c2); free(o.l1); free(o.l2);
  fclose(pseudo);
  fclose(python);
  fclose(timef);
  fclose(file);
  fclose(tecC_file);
  fclose(tecL_file);
  return 0;
}
