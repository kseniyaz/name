#include <stdio.h>
#include <math.h>
#include <string.h >
#include <time.h>
#define f1 1575420000
#define f2 1227600000
#define speed_of_light 299792458
#define pi  3.1415926535

int main(void) {
//    srand(time(NULL));
    FILE *file;
    FILE *tecC_file;
    FILE *tecL_file;
    FILE *timef;
    FILE *python;
    FILE *pseudo;
    pseudo = fopen("pseudo.txt", "w");
    python = fopen("for_python.txt", "w");
    timef = fopen("time.txt", "w");
    tecC_file = fopen("tecC.txt", "w");
    tecL_file = fopen("tecL.txt", "w");
	file = fopen("vost_0618a.16o", "r");

	char str[82];
	if (file == NULL)       printf("isn't open");

	for (int i=0; i<76; i++)
    {
        fgets(str, 81 , file);
    }

    int sum_sec[2*60*24] = {0};

	struct date {
        int year, month, day, hours, minutes, seconds;
    };
    struct date time[2*60*24];

    char *sat_name_s[28];

    double tecC[2*60*24] = {0}, tecL[2*60*24] = {0} ;
	double c1, p1, l1, c2, p2, l2;
	int sat_numb;
	char sat_numb_str[3], sat_name[4];
    for (int j=0; j < 2*60*24; j++)
    {
        puts(str);
        sscanf(str, "%d%d%d%d%d%d", &time[j].year, &time[j].month, &time[j].day, &time[j].hours, &time[j].minutes, &time[j].seconds);
        printf("Time: %d.%d.%d    %d:%d:%d.00\n\n",  time[j].day, time[j].month, time[j].year, time[j].hours, time[j].minutes, time[j].seconds);
        sum_sec[j] += 60*60*time[j].hours + 60*time[j].minutes + time[j].seconds;
        sat_numb_str[0] = str[30];
        sat_numb_str[1] = str[31];
//        sat_numb_str[2] = "\0";
        sscanf(sat_numb_str, "%d", &sat_numb);
        printf("number of satellites %d\n\n", sat_numb);

        if (time[j].year != 16)   break;


        for (int i = 0; i < 12; i++)
        {
            sat_name[0] = str[32+3*i];
            sat_name[1] = str[33+3*i];
            sat_name[2] = str[34+3*i];
//            sat_name[3] = "\0";
            //sat_name_s[i] = sat_name;
            sat_name_s[i] = malloc(strlen(sat_name));
            strcpy (sat_name_s[i],sat_name);
        }
        fgets(str, 81, file);
        fgets(str, 81, file);
        puts(str);
        for (int i = 0; i < (sat_numb-12)%13; i++)
        {
            sat_name[0] = str[32+3*i];
            sat_name[1] = str[33+3*i];
            sat_name[2] = str[34+3*i];
//            sat_name[3] = "\0";
            //sat_name_s[i+12] = sat_name;
            sat_name_s[i+12] = malloc(strlen(sat_name));
            strcpy (sat_name_s[i+12],sat_name);
        }

        if (sat_numb >= 25)     {
                //fgets(str, 81, file);
                fgets(str, 81, file);
                puts(str);
                for (int i = 0; i < sat_numb - 24; i++)
                {
                    sat_name[0] = str[32+3*i];
                    sat_name[1] = str[33+3*i];
                    sat_name[2] = str[34+3*i];
//                    sat_name[3] = "\0";
                    //sat_name_s[i+24] = sat_name;
                    sat_name_s[i+24] = malloc(strlen(sat_name));
                    strcpy (sat_name_s[i+24],sat_name);
                }

        }

        for (int i = 0; i < sat_numb; i++)
        {
                printf("test %d %.3s \n", i+1, sat_name_s[i]);
        }

        for (int i=0; i<sat_numb; i++)
        {
          c1 = p1 = l1 = c2 = p2 = l2 = 0;
          fgets(str, 81, file);
          sscanf(str, "%15lf     %15lf   %15lf    %*15lf    %15lf    %15lf", &c1, &p1, &l1, &c2, &p2);
          fgets(str, 81, file);
          sscanf(str, "%15lf     %*15lf", &l2);
          if (fabs(p2) < 10e-7) {
            continue;
          }

          //for GPS satellites
          if (sat_name_s[i][0] == 'G')
          {
              printf("GPS satellite number %s\n", sat_name_s[i]);
              fprintf(pseudo, "%s %lf %7d\n",sat_name_s[i], p1, sum_sec[j]);
              printf("c1: %.3lf\np1: %.3lf\nl1: %.3lf\n\nc2: %.3lf\np2: %.3lf\nl2: %.3lf\n",c1, p1, l1, c2, p2, l2);
              printf("-----\n");
              tecC[i] = ((p2-p1) / 40.308 *(f1 *f1 * f2 *f2) / (f1 *f1 - f2 *f2) );
              printf("\ntec (P2-P1) =    %lf\n\n",tecC[i]);
              fprintf(tecC_file, "%lf\n",tecC[i]);
              tecL[i] = ((l1*speed_of_light / f1-l2*speed_of_light / f2) / 40.308 *(f1 *f1 * f2 *f2) / (f1 *f1 - f2 *f2) );

              //printf("\n\n%lf\n\n",40.3*pow(10,16)/f1/f1*tecL[i]/0.1905);
              /*if (tecL[i] > 20)
              {
                  tecL[i] = tecL[i] - (int)(tecL[i] / 40.308 * (f1 *f1 * f2 *f2) / (f1 *f1 - f2 *f2))*40.308 / (f1 *f1 * f2 *f2) * (f1 *f1 - f2 *f2);
              }*/
              printf("\ntec (L1-L2) =    %lf\n\n",(tecL[i]));
              fprintf(tecL_file, "%lf\n",tecL[i]);
              fprintf(python, "%s %9lf %12lf %.3lf %.3lf %.3lf %.3lf %.3lf %.3lf %d.%.2d.20%d %d:%d:%d\n", sat_name_s[i], tecC[i], tecL[i],c1, p1, l1, c2, p2, l2, time[j].day, time[j].month, time[j].year, time[j].hours, time[j].minutes, time[j].seconds);
          }

          //for GLONASS satellites

          int freq;
          char fff[2];
          if (sat_name_s[i][0] == 'R')
          {
              printf("GLONASS satellite number %s\n", sat_name_s[i]);
              fff[0] = sat_name_s[i][1];
              fff[1] = sat_name_s[i][2];
              sscanf(fff, "%d", &freq);
              int f1glo = 1600995000 + freq*5625000;
              int f2glo = 1248060000 + freq*4375000;
              printf("frequency. f1 = %d; f2 = %d\n",f1glo, f2glo);
              printf("c1: %.3lf\np1: %.3lf\nl1: %.3lf\n\nc2: %.3lf\np2: %.3lf\nl2: %.3lf\n",c1, p1, l1, c2, p2, l2);
              printf("-----\n");
              tecC[i] = ((p2-p1) / 40.308 *(f1glo *f1glo * f2glo *f2glo) / (f1glo *f1glo - f2glo *f2glo) );
              printf("\ntec (P2-P1) =    %lf\n\n",tecC[i]);
              fprintf(tecC_file, "%lf\n",tecC[i]);
              tecL[i] = ((l1 * speed_of_light / f1glo - l2 * speed_of_light / f2glo) / 40.308 *(f1glo *f1glo * f2glo *f2glo) / (f1glo *f1glo - f2glo *f2glo));
              //printf("\n\n%lf\n\n",40.3*pow(10,16)/f1glo/f1glo*tecL[i]/0.1905);
              if (fabs(tecL[i]) > 20)
              {
                  /*while ((fabs(tecL[i]) > 20) && (tecL[i] > 0.000) || (tecL[i] < 0.000))
                  {
                      if ((tecL[i] > 0))
                      {

                          tecL[i] -= 1 / 40.308 *(f1glo *f1glo * f2glo *f2glo) / (f1glo *f1glo - f2glo *f2glo);

                      }
                      else
                      {

                           tecL[i] += 1 / 40.308 *(f1glo *f1glo * f2glo *f2glo) / (f1glo *f1glo - f2glo *f2glo);
                      }
                      printf("tec %lf\n\n",tecL[i]);*/

                  /*int jjj = 0;
                  while ((fabs(tecL[i]) > 20) && (tecL[i] > 0.000) || (tecL[i] < 0.000))
                  {
                      if ((tecL[i] > 0)&&jjj<100)
                      {
                          jjj++;
                          //printf("%d %d", rand()%10, rand()%10);
                          //l1 -= speed_of_light / f1glo; // /(1/ 40.308 *(f1glo *f1glo * f2glo *f2glo) / (f1glo *f1glo - f2glo *f2glo));
                          //l2 -= speed_of_light / f2glo; // /(1/ 40.308 *(f1glo *f1glo * f2glo *f2glo) / (f1glo *f1glo - f2glo *f2glo));
                          tecL[i] = (((rand()%50+1)*l1*speed_of_light / (f1glo) - (rand()%50+1)*l2*speed_of_light / f2glo) / 40.308 *(f1glo *f1glo * f2glo *f2glo) / (f1glo *f1glo - f2glo *f2glo));

                      }
                      else
                      {
                          jjj++;
                           tecL[i] = (((rand()%50+1)*l1*speed_of_light / (f1glo) - (rand()%50+1)*l2*speed_of_light / f2glo) / 40.308 *(f1glo *f1glo * f2glo *f2glo) / (f1glo *f1glo - f2glo *f2glo));
                           if (jjj>100)  break;
                      }
                      printf("tec %lf\n\n",tecL[i]);
                  }
                    */
                  //tecL[i] = tecL[i] - (int)(tecL[i]*100 / 40.308 *(f1glo *f1glo * f2glo *f2glo) / (f1glo *f1glo - f2glo *f2glo))*0.40308 /(f1glo *f1glo * f2glo *f2glo) * (f1glo *f1glo - f2glo *f2glo);
              }
              printf("\ntec (L1-L2) =    %lf\n\n",(tecL[i]));
              fprintf(tecL_file, "%lf\n",tecL[i]);
              fprintf(python, "%s %9lf %12lf %.3lf %.3lf %.3lf %.3lf %.3lf %.3lf %d.%.2d.20%d %d:%d:%d\n", sat_name_s[i], tecC[i], tecL[i],c1, p1, l1, c2, p2, l2, time[j].day, time[j].month, time[j].year, time[j].hours, time[j].minutes, time[j].seconds);
          }
        }

        fprintf(timef, "%d\n",sum_sec[j]);
        fgets(str, 81, file);

  }
  fclose(pseudo);
  fclose(python);
  fclose(timef);
  fclose(file);
  fclose(tecC_file);
  fclose(tecL_file);
  return 0;
}
