#include <stdio.h>
#include <math.h>
#define f1 1575420000
#define f2 1227600000
#define speed_of_light 299792458
#define f1glo 1600995000 //+ n*5625000
#define f2glo 1248060000 //+ n*4375000, n - ����� ���������� ������

int main() {

    FILE *file;
    FILE *tecC_file;
    FILE *tecL_file;
    FILE *timef;
    timef = fopen("time.txt", "w");
    tecC_file = fopen("tecC.txt", "w");
    tecL_file = fopen("tecL.txt", "w");
	file = fopen("vost_0616a.16o", "r");
	char str[82];
	if (file == NULL)       printf("isn't open");

	for (int i=0; i<76; i++)
    {
        fgets(str, 81 , file);
    }

    int sum_sec[2*60*24] = {0};

	struct date {
        int year, month, day, hours, minutes, seconds;
    };
    struct date time[2*60*24];

    //char *Names[] = { "G 1", "G 2", "G 3","G 4","G 5", "G 6", "G 7", "G 8","G 9", "G10", "G11", "G12", "G13", "G15","G16", "G17", "G18", "G19","G20","G21","G22","G23","G24","G25","G26","G27","G28","G29","G30","G31","G32"};
    //char sat_name_s[26];
    double tecC[56] = {0}, tecL[56] = {0} ;
	double c1, p1, l1, c2, p2, l2;
	int sat_numb;
	char sat_numb_str[3];
    for (int j=0; j < 2*24*60; j++)
    {
        puts(str);
        sscanf(str, "%d%d%d%d%d%d", &time[j].year, &time[j].month, &time[j].day, &time[j].hours, &time[j].minutes, &time[j].seconds);
        printf("Time: %d.%d.%d    %d:%d:%d.00\n\n",  time[j].day, time[j].month, time[j].year, time[j].hours, time[j].minutes, time[j].seconds);
        sum_sec[j] += 60*60*time[j].hours + 60*time[j].minutes + time[j].seconds;
        sat_numb_str[0] = str[30];
        sat_numb_str[1] = str[31];
        sat_numb_str[2] = "\0";
        sscanf(sat_numb_str, "%d", &sat_numb);
        printf("number of satellites %d\n\n", sat_numb);

        if (time[j].year != 16)   break;


        if (sat_numb >= 25)     {
                fgets(str, 81, file);
            }


        for (int i=0; i<sat_numb; i++)
        {
          c1 = p1 = l1 = c2 = p2 = l2 = 0;
          fgets(str, 81, file);
          sscanf(str, "%15lf     %15lf   %15lf    %*15lf    %15lf    %15lf", &c1, &p1, &l1, &c2, &p2);
          fgets(str, 81, file);
          sscanf(str, "%15lf     %*15lf", &l2);
          if (fabs(p2) < 10e-7) {
            continue;
          }

          printf("c1: %.3lf\np1: %.3lf\nl1: %.3lf\n\nc2: %.3lf\np2: %.3lf\nl2: %.3lf\n",c1, p1, l1, c2, p2, l2);
          printf("-----\n");
          tecC[i] = ((p2-p1) / 40.308 *(f1 *f1 * f2 *f2) / (f1 *f1 - f2 *f2) );
          printf("\ntec (P2-P1) =    %lf\n\n",tecC[i]);
          fprintf(tecC_file, "%lf\n",tecC[i]);
          tecL[i] = ((l1*speed_of_light / f1-l2*speed_of_light / f2) / 40.308 *(f1 *f1 * f2 *f2) / (f1 *f1 - f2 *f2) );
          printf("\ntec (L1-L2) =    %lf\n\n",(tecL[i]));
          fprintf(tecL_file, "%lf\n",tecL[i]);

          /*
          printf("\ntec (L1-L2) =    %lf\n\n",(int)(tecL[i]/21));
          ����� � �������� ��������� tecC � tecL. what??
          */
        }

        fprintf(timef, "%d\n",sum_sec[j]);
        fgets(str, 81, file);
        fgets(str, 81, file);
        fgets(str, 81, file);
  }

  fclose(timef);
  fclose(file);
  fclose(tecC_file);
  fclose(tecL_file);
  return 0;
}
