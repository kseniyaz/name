#include <stdio.h>
#include <math.h>
#include <string.h >
#include <stdlib.h>
#include <gmp.h>
#include <assert.h>
#include <limits.h>
#define ugle 0.000072921151467
#define speed_of_light 299792458
#define pi  3.1415926535

double ex_an(double e, double M, double eps)
{
  double E = 0;
  double rez = 100;
  while (abs(rez - E) > eps)
    {
        rez = E;
        E = M + e*sin(E);
    }

  return E;
}

int main(void) {

    int number[400], year[400], month[400], day[400], hour[400], minute[400], seconds[400], time[4], p[4], dtt[4];
    double IODE[400],Crs[400], delta_n[400], M0[400], popr[400];
    double Cus[400], e[400], Cuc[400], A[400];
    double Toe[400], Cic[400], Omega0[400], Cis[400];
    double i0[400], Crc[400], w0[400], Omega_p[400];
    double IDOT[400], WN[400];

    double mu = 19964980.3857;
    double v[400], r[400], ii[400], u[400];
    double MS[400], E[400];
    double n0[400], n[400],  f[400];
    double Tem[400], t[400];
    double delu[400], delr[400], deli[400];
    double  Xorb[400], Yorb[400], x[400], y[400], z[400];
    double  ugl[400],  ugl0[400], uglp[400];

    p[0] = 24523248.509000,time[0] = 0;
    dtt[0] = 0.000447783154;
    IODE[0] = 52.000000000000, Crs[0] = 117.343750000000, delta_n[0] = 0.000000004340, M0[0] = 2.072583821355;
    Cuc[0] = 0.000006034970, e[0] = 0.009586923406, Cus[0] = 0.000005092472, A[0] = 5153.647415161000;
    Toe[0] = 518400.000000000000, Cic[0] = 0.000000089990, Omega0[0] = 0.621355911682, Cis[0] = 0.000000011176;
    i0[0]= 0.966403016073, Crc[0] = 284.593750000000, w0[0]= -2.671105733773, Omega_p[0] = -0.000000008151;
    IDOT[0] = 0.000000000266, WN[0] = 1901.000000000000;

    p[1] = 24624152.393000, time[1] = 120;
    dtt[1] = 0.000447765458;
    IODE[1] = 56.000000000000, Crs[1] = 116.406250000000, delta_n[1] = 0.000000004211, M0[1] = 3.122768915083;
    Cuc[1] = 0.000005943701, e[1] = 0.009586877655, Cus[1] = 0.000005098060, A[1] = 5153.647565842000;
    Toe[1] = 525600.000000000000, Cic[1] = 0.000000004901, Omega0[1] = 0.621297879185, Cis[1] = 0.000000091270;
    i0[1]  = 0.966405981408, Crc[1] = 285.281250000000, w0[1] = -2.671095645489, Omega_p[1] = -0.000000007944;
    IDOT[1] = 0.000000000288, WN[1] = 1901.000000000000;

    p[2] = 21144598.855000;time[2] = 120;
    dtt[2] = -0.000039433129;
    IODE[2] = 21.000000000000, Crs[2] = -85.093750000000, delta_n[2] = 0.000000004239, M0[2] = 0.524195047353;
    Cuc[2] = -0.000004416332, e[2] = 0.001715348917, Cus[2] = 0.000009685755, A[2] = 5153.710113525001;
    Toe[2] = 525600.000000000000, Cic[2] = 0.000000003039, Omega0[2] = 2.685642105579, Cis[2] = 0.000000033528;
    i0[2] = 0.963748766267, Crc[2] = 191.406250000000, w0[2] = -1.071054508119, Omega_p[2] = -0.000000007885;
    IDOT[2] = 0.000000000109, WN[2] = 1901.000000000000;

    p[3] =  22555804.561000;time[3] = 120;
    dtt[3] = -0.000315734185;
    IODE[3] = 50.000000000000, Crs[3] = 24.875000000000, delta_n[3] = 0.000000004918, M0[3] = -1.134146980106;
    Cuc[3] = 0.000001238659, e[3] = 0.008163801860, Cus[3] = 0.000011047348, A[3] = 5153.614196777000;
    Toe[3] = 525600.000000000000, Cic[3] = 0.000000004891, Omega0[3] = 0.509456677281, Cis[3] = 0.000000098720;
    i0[3] = 0.930795255016, Crc[3] = 151.593750000000, w0[3] = 0.482194606392, Omega_p[3] = -0.000000008076;
    IDOT[3] = -0.000000000546, WN[3] = 1901.000000000000;
    day[0],day[1], day[3], day[2] = 18,18,18,18;
    year[0],year[1],year[2],year[3] = 16,16,16,16;
    month[0],month[1],month[2],month[3] = 06,06,06,06;

    for (int j = 0; j < 4; j++)
    {
            n0[j] = mu/pow(A[j],3);
            Tem[j] = time[j] - p[j]/speed_of_light + (24 * 60 * 60)*((day[j]  + ((31 * (month[j] + 12)) / 12) + year[j]+2000 + ((2000+year[j])/ 4) - ((2000+year[j]) / 100) + ((year[j]+2000) / 400))%7);
            t[j] = Tem[j] - Toe[j];
            if (t[j] > 302400)  t[j] = t[j] - 604800;
            else if (t[j] < -302400)    t[j] = t[j] + 604800;
            n[j] = n0[j] + delta_n[j];
            printf("Tem %lf\nt %lf\n", Tem[j],t[j]);
            MS[j] = M0[j] + n[j]*t[j];
            if (MS[j] > 2*pi)
            {
                while(MS[j] > 2*pi)
                {
                    MS[j] = MS[j] - (2*pi);

                }
            }
            if (MS[j] < -2*pi)
            {
                while(MS[j] < -2*pi)
                {
                    MS[j] = MS[j] + (2*pi);

                }
            }
            E[j] = ex_an(e[j], MS[j], 0.00000001);
            v[j] = 2*atan(pow(((1+e[j])/(1-e[j])),(1/2))*(tan(E[j]/2)));
            f[j] = v[j] + w0[j];
            printf("n %lf\nM %lf\nE %lf\nv %.9lf\nf %lf\n\n",n[j], MS[j], E[j], v[j], f[j]);
            delu[j] = Cus[j]*sin(2*f[j])+Cuc[j]*cos(2*f[j]);
            delr[j] = Crs[j]*sin(2*f[j])+Crc[j]*cos(2*f[j]);
            deli[j] = Cis[j]*sin(2*f[j])+Cic[j]*cos(2*f[j]);

            u[j] = f[j] + delu[j];
            r[j] = A[j]*A[j]*(1 - e[j]*cos(E[j])) + delr[j];
            ii[j] = i0[j] + deli[j] + IDOT[j]*t[j];

            Xorb[j] = r[j]*cos(u[j]);
            Yorb[j] = r[j]* sin(u[j]);

            ugl[j] = Omega0[j] +t[j]*(Omega_p[j] - ugle) - ugle*Toe[j];

            x[j] = Xorb[j]*cos(ugl[j]) - Yorb[j]*cos(ii[j])*sin(ugl[j]);
            y[j] = Xorb[j]*sin(ugl[j]) + Yorb[j]*cos(ii[j])*cos(ugl[j]);
            z[j] = Yorb[j]*sin(ii[j]);

            printf("%8d \nx = %16lf\ny = %16lf\nz = %16lf\n",time[j],x[j], y[j], z[j]);
    }
    double dp[4], dx[4], dy[4], dz[4], sp[4], sx[4], sy[4], sz[4],pp[4], xn[4], yn[4],zn[4];
    double a[3], D, D1, D2, D3, dd1, dd2, dd3;
    double xsh, ysh, zsh, ax, ay, az;
    double dx4sh, dy4sh, dz4sh;
    double a4, b4, c4, epsilon, eps1, eps2, dt1, dt2;
    double xx, yy, zz;
    double ee, eesh, aa = 6378137.0, bb = 6356752.0;
    double lambda, teta, phi;
    int m = 0;
    //double xn[] = {21277632.227,14136001.009,6380947.343,13667726.877};
    //double yn[] = {2624628.197,-7530688.663,22063437.865,9545209.321};
    //double zn[] = {15798292.012,21144548.488,13292356.357,21346910.550};
    //double pp[] = {21267472.262,21406610.202,22193724.917,20826905.858};
    //double dtt[] = {-0.000071967360017,0.0000863327302812,0.0000760678518091,0.0000573110823339};
    for (int i = 0; i < 3; i++)
    {
        pp[i] = pp[i] + dtt[i]*speed_of_light;
        xn[i] = x[i];
        yn[i] = y[i];
        zn[i] = z[i];
    }
    for (int i = 0; i < 3; i++)
    {
        dp[i] = (pp[i+1] - pp[i]);
        dx[i] = (xn[i+1] - xn[i]);
        dy[i] = (yn[i+1] - yn[i]);
        dz[i] = (zn[i+1] - zn[i]);
        sp[i] = (pp[i+1] + pp[i]);
        sx[i] = (xn[i+1] + xn[i]);
        sy[i] = (yn[i+1] + yn[i]);
        sz[i] = (zn[i+1] + zn[i]);

    }
    for (int i= 0; i < 3; i++)
     {
        a[i] = (dx[i] * sx[i] + dy[i] * sy[i] + dz[i] * sz[i] - dp[i] * sp[i]) / 2;

     }

    D = dx[0]*dy[1]*dz[2] + dy[0]*dz[1]*dx[2]  + dx[1]*dy[2]*dz[0]  - dz[0]*dy[1]*dx[2] - dx[1]*dy[0]*dz[2] - dy[2]*dz[1]*dx[0] ;
    D1 = a[0]*dy[1]*dz[2] + dy[0]*dz[1]*a[2] + a[1]*dy[2]*dz[0] - dz[0]*dy[1]*a[2] - a[1]*dy[0]*dz[2] - dy[2]*dz[1]*a[0] ;
    D2 = dx[0]*a[1]*dz[2] + a[0]*dz[1]*dx[2] + dx[1]*a[2]*dz[0] - dz[0]*a[1]*dx[2] - dx[1]*a[0]*dz[2] - a[2]*dz[1]*dx[0];
    D3 = dx[0]*dy[1]*a[2] + dy[0]*a[1]*dx[2] + dx[1]*dy[2]*a[0] - a[0]*dy[1]*dx[2] - dx[1]*dy[0]*a[2] - dy[2]*a[1]*dx[0];
    dd1 = -(dp[0]*dy[1]*dz[2] + dy[0]*dz[1]*dp[2] + dp[1]*dy[2]*dz[0] - dz[0]*dy[1]*dp[2] - dp[1]*dy[0]*dz[2] - dy[2]*dz[1]*dp[0]) ;
    dd2 = -(dx[0]*dp[1]*dz[2] + dp[0]*dz[1]*dx[2] + dx[1]*dp[2]*dz[0] - dz[0]*dp[1]*dx[2] - dx[1]*dp[0]*dz[2] - dp[2]*dz[1]*dx[0]);
    dd3 = -(dx[0]*dy[1]*dp[2] + dy[0]*dp[1]*dx[2] + dx[1]*dy[2]*dp[0] - dp[0]*dy[1]*dx[2] - dx[1]*dy[0]*dp[2] - dy[2]*dp[1]*dx[0]);
    xsh = D1/D; ysh = D2/D; zsh = D3/D;

    ax = dd1/D; ay = dd2/D; az = dd3/D;
    dx4sh = xn[3] - xsh;
    dy4sh = yn[3] - ysh;
    dz4sh = zn[3] - zsh;
    a4 = 1 - ax*ax - ay*ay - az*az;
    b4 = pp[3] + dx4sh*ax + dy4sh*ay + dz4sh*az;
    c4 = - dx4sh*dx4sh - dy4sh*dy4sh - dz4sh*dz4sh + pp[3]*pp[3];
    c4 = c4/2;
    //printf("%lf\n%lf \n\n",b4,c4);
    double mm = 0;
    mm = sqrt((b4*b4 - a4*c4)/(a4*a4));
    //printf("%lf mm\n\n",mm);
    eps1 = - b4/a4 + mm;
    eps2 = - b4/a4 - mm;

    dt1 = eps1/speed_of_light;
    dt2 = eps2/speed_of_light;
    if (abs(dt1) <= abs(dt2))	epsilon = eps1;
    else	epsilon = eps2;
    xx = xsh + epsilon*ax;
    yy = ysh + epsilon*ay;
    zz = zsh + epsilon*az;
    printf("\n\nx = %lf\ny = %lf\nz = %lf\n",xx,yy,zz);
    ee = (aa * aa - bb * bb) / (aa * aa);
    eesh = (aa * aa - bb * bb) / (bb * bb);
    //printf("%lf\n%lf \n\n",ee,eesh);
    lambda = atan2(yy , xx)* 180.0 / pi;
    teta = atan2 (aa * zz , (bb * pow((xx * xx + yy * yy),0.5)))* 180.0 / pi;
    phi = atan2 ((zz + eesh * eesh * bb * pow((sin(teta)),3)) , (pow((xx * xx + yy * yy),0.5)- ee * aa * pow(cos(teta),3))) * 180.0 / pi;

    printf("lam = %lf\nphi = %lf\n",lambda,  phi);
}
